export interface User {
  uid: string;
  email: string;
  darkMode: boolean;
}

export interface CalendarDay {
  number: number;
  year: number;
  monthIndex: number;
  weekDayIndex: number;
  uid: string;
  empty: boolean;
}

export interface Day {
  uid: string;
  text: string;
  images: { db: string; uid: string }[];
  created: boolean;
}

export interface SelectedDate {
  month: string;
  monthIndex: number;
  day: number;
  year: number;
  uid: string;
}
