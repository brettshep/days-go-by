import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  WrappedValue,
} from "@angular/core";
import { Day, SelectedDate } from "../../../../interfaces";
import { Subscription, Subject } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  withLatestFrom,
} from "rxjs/operators";

@Component({
  selector: "app-editor",
  template: `
    <div class="maxWidth">
      <h2>
        {{ selectedDate.year }}
      </h2>
      <h1>
        {{ selectedDate.month }} {{ selectedDate.day }}
        <button>
          <input type="file" (change)="onFileChange($event)" />
          <i class="fas fa-image"></i>
        </button>
      </h1>
      <section>
        <div class="spinner" *ngIf="_day === null"></div>
        <ng-container *ngIf="_day">
          <textarea
            customTextArea
            placeholder="Today..."
            (onInput)="onTextUpdate($event)"
            [value]="_day.text || ''"
            [setValue]="updateTextArea"
          ></textarea>
          <div class="imgCont" *ngFor="let image of _day.images; index as i">
            <button
              class="delete"
              (click)="deleteImage.emit({ day: _day, index: i })"
            >
              <i class="fas fa-times"></i>
            </button>
            <img
              src="{{ image.db }}"
              alt="image"
              *ngIf="image.db; else loading"
            />
            <ng-template #loading>
              <div class="spinner"></div>
            </ng-template>
          </div>
        </ng-container>
      </section>
    </div>
  `,
  styleUrls: ["./editor.component.sass"],
})
export class EditorComponent implements OnInit {
  @Input() userID: string;
  @Input() selectedDate: SelectedDate;
  @Input() set day(val: Day) {
    // update day value
    this._day = val;

    // unsub from old text updates
    if (val === null) return;

    // unsub from old form
    if (this.textChangeSub) this.textChangeSub.unsubscribe();

    // save text changes
    this.subToTextChange();

    // update textarea if text
    if (val && val.text) {
      this.updateTextArea.next(val.text);
    } else {
      this.updateTextArea.next("");
    }
  }

  @Output() deleteImage = new EventEmitter<{ day: Day; index: number }>();
  @Output() onUpdateDay = new EventEmitter<{ day: Day; file: File }>();
  @Output() onNewDay = new EventEmitter<{ day: Day; file: File }>();

  _day: Day;
  textChangeSub: Subscription;
  text$: Subject<string> = new Subject<string>();
  updateTextArea = new Subject<string>();

  constructor() {}

  ngOnInit(): void {}

  subToTextChange() {
    // subscribe, only updating textchange every second
    this.textChangeSub = this.text$
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((text) => {
        if (this._day.created) {
          this.updateDay(this._day, text, null);
        } else {
          this.createNewDay(this._day, text, null);
        }
      });
  }

  ngOnDestroy() {
    this.textChangeSub.unsubscribe();
  }

  onFileChange(e: any) {
    const files = e.target.files;
    //see if file
    if (files && files[0]) {
      const file = files[0];
      const imageType = /image.*/;
      //see if image file
      if (file.type.match(imageType)) {
        // update day
        if (this._day.created) {
          this.updateDay(this._day, null, file);
        }
        // new day
        else {
          this.createNewDay(this._day, null, file);
        }
        e.target.value = "";
      }
    }
  }

  onTextUpdate(e: string) {
    this.text$.next(e);
  }

  updateDay(day: Day, text?: string, image?: File) {
    if (image) {
      day.images.push({ db: "", uid: "" });
      this.onUpdateDay.emit({ day, file: image });
    } else {
      day.text = text;
      this.onUpdateDay.emit({ day, file: null });
    }
  }

  createNewDay(day: Day, text?: string, image?: File) {
    const dayID = this.selectedDate.uid;

    if (image) {
      day = {
        uid: dayID,
        text: "",
        images: [{ db: "", uid: "" }],
        created: true,
      };
      this.onNewDay.emit({ day, file: image });
    } else {
      day = {
        uid: dayID,
        text,
        images: [],
        created: true,
      };
      this.onNewDay.emit({ day, file: null });
    }
  }
}
