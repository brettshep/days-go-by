import { Subject, Subscription } from "rxjs";
// -----------textarea.directive.ts------------

import {
  Directive,
  ElementRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from "@angular/core";

@Directive({
  selector: "[customTextArea]",
})
export class TextAreaDirective {
  @Input() setValue: Subject<string>;
  @Output() onInput = new EventEmitter<string>();
  sub: Subscription;

  constructor(private elemRef: ElementRef) {}

  ngAfterViewInit() {
    this.sub = this.setValue.subscribe((val) => {
      this.textarea.value = val;
      this.resize();
    });

    this.resize();

    // setup input resize listener
    this.textarea.addEventListener("input", (e: KeyboardEvent) => {
      this.onInput.emit(this.textarea.value);
      this.resize();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  @HostListener("window:resize", ["$event"])
  resize() {
    if (this.textarea.scrollHeight > this.textarea.clientHeight) {
      this.textarea.style.height = this.textarea.scrollHeight + "px";
    } else {
      this.textarea.style.height = 0 + "px";
      this.textarea.style.height = this.textarea.scrollHeight + "px";
    }
  }

  get textarea() {
    return this.elemRef.nativeElement as HTMLTextAreaElement;
  }
}
