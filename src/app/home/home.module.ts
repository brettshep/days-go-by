import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";
import { EditorComponent } from "./editor/editor.component";
import { CalendarComponent } from "./calendar/calendar.component";
import { TextAreaDirective } from "./textarea.directive";
import { DarkModeToggleComponent } from './dark-mode-toggle/dark-mode-toggle.component';

export const ROUTES: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  { path: "**", redirectTo: "/home" },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [
    HomeComponent,
    EditorComponent,
    CalendarComponent,
    TextAreaDirective,
    DarkModeToggleComponent,
  ],
})
export class HomeModule {}
