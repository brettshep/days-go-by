import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { CalendarDay } from "../../../../interfaces";

@Component({
  selector: "calendar",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="calendar">
      <!-- Month/Year -->
      <div class="controls">
        <button (click)="onNextMonth(-1)">
          <i class="fas fa-chevron-left"></i>
        </button>
        <h2>{{ date }}</h2>
        <button (click)="onNextMonth(1)">
          <i class="fas fa-chevron-right"></i>
        </button>
      </div>

      <!-- weekdays -->
      <div class="grid">
        <div
          *ngFor="let weekDay of weekDaysName; index as i"
          class="block weekday"
        >
          {{ weekDay }}
        </div>
      </div>
      <!-- days of month -->
      <div class="daysGrid">
        <div class="spinner" *ngIf="daysOfMonthComplete === null"></div>
        <div class="grid" *ngIf="daysOfMonthComplete">
          <div
            *ngFor="let day of daysOfMonth"
            class="block"
            [ngClass]="{
              selectedDay: day.uid === selectedDayUID,
              empty: day.empty
            }"
          >
            <!-- regular day -->
            <span
              class="day notEmpty"
              [class.complete]="daysOfMonthComplete[day.number]"
              *ngIf="!day.empty; else empty"
              (click)="onSelectDay.emit(day.number)"
            >
              {{ day.number }}
            </span>

            <!-- empty day -->
            <ng-template #empty>
              <span class="day" class="empty">
                {{ day.number }}
              </span>
            </ng-template>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./calendar.component.sass"],
})
export class CalendarComponent implements OnInit {
  @Input() date: string;
  @Input() daysOfMonth: CalendarDay[];
  @Input() selectedDayUID: string;
  @Input() daysOfMonthComplete: { [key: string]: boolean };

  @Output() onSelectDay = new EventEmitter<string>();
  @Output() changeMonth = new EventEmitter<number>();

  weekDaysName = ["S", "M", "T", "W", "T", "F", "S"];

  ngOnInit(): void {}

  onNextMonth(i: number): void {
    this.changeMonth.emit(i);
  }
}
