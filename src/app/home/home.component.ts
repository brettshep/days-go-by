import { Component, OnInit } from "@angular/core";
import { DayService } from "../services/day.service";
import { Day, CalendarDay, SelectedDate } from "../../../interfaces";
import { CalendarService } from "../services/calendar.service";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { map } from "rxjs/operators";

@Component({
  selector: "app-home",
  template: `
    <div class="container">
      <button class="logout" (click)="logout()">Logout</button>
      <dark-mode-toggle
        [darkMode]="darkMode$ | async"
        (toggle)="toggleDarkMode($event)"
      >
      </dark-mode-toggle>
      <div class="maxWidth">
        <calendar
          [daysOfMonth]="daysOfMonth"
          [date]="currCalendarDate"
          [selectedDayUID]="selectedDate.uid"
          [daysOfMonthComplete]="daysOfMonthComplete$ | async"
          (changeMonth)="onNextMonth($event)"
          (onSelectDay)="selectNewDay($event)"
        ></calendar>
        <div class="calendarSpace"></div>
        <app-editor
          [day]="currDay$ | async"
          [selectedDate]="selectedDate"
          [userID]="$userID | async"
          (onUpdateDay)="updateDay($event)"
          (onNewDay)="newDay($event)"
          (deleteImage)="deleteImage($event)"
        >
        </app-editor>
      </div>
    </div>
  `,
  styleUrls: ["./home.component.sass"],
})
export class HomeComponent implements OnInit {
  monthIndex: number;
  year: number;
  currCalendarDate: string;
  daysOfMonth: CalendarDay[];

  currDay$: Observable<Day>;
  daysOfMonthComplete$: Observable<{ [key: string]: boolean }>;
  darkMode$: Observable<boolean>;
  selectedDate: SelectedDate;
  $userID: Observable<string>;

  constructor(
    private dayServ: DayService,
    private calServ: CalendarService,
    private authServ: AuthService
  ) {}

  ngOnInit() {
    // fetch darkmode
    this.darkMode$ = this.authServ.fetchDarkMode();

    this.$userID = this.authServ.authState.pipe(map((user) => user.uid));

    const date = new Date();
    this.year = date.getFullYear();
    this.monthIndex = date.getMonth();
    this.selectNewDay(date.getDate());
    this.setCurrCalendarDate();
    this.setdaysOfMonth();
  }

  toggleDarkMode(e) {
    this.authServ.toggleDarkMode(e);
  }

  async selectNewDay(dayOfMonth: number) {
    // udpdate selected date
    const monthName = this.calServ.monthName[this.monthIndex];
    this.selectedDate = {
      day: dayOfMonth,
      month: monthName,
      monthIndex: this.monthIndex,
      year: this.year,
      uid: this.selectedDateUID(
        dayOfMonth,
        this.monthIndex,
        this.year,
        monthName
      ),
    };

    // get stuff from db
    this.currDay$ = this.dayServ.fetchNewDay(this.selectedDate.uid);
  }

  logout() {
    this.authServ.logout();
  }

  // --------Day CRUD----------
  updateDay(data: { day: Day; file: File }) {
    this.dayServ.updateDay(
      data.day,
      data.file,
      this.monthUID,
      this.selectedDate.day
    );
  }

  newDay(data: { day: Day; file: File }) {
    this.dayServ.newDay(
      data.day,
      data.file,
      this.monthUID,
      this.selectedDate.day
    );
  }

  deleteImage(data: { day: Day; index: number }) {
    this.dayServ.deleteImage(
      data.day,
      data.index,
      this.monthUID,
      this.selectedDate.day
    );
  }

  // ----------CALENDAR MANAGEMENT-------
  setCurrCalendarDate() {
    this.currCalendarDate = `${this.calServ.monthName[this.monthIndex]} ${
      this.year
    }`;

    // get completed days from firebase
    this.daysOfMonthComplete$ = this.dayServ.fetchDaysOfMonthCompleted(
      this.monthUID
    );
  }

  setdaysOfMonth() {
    this.daysOfMonth = this.calServ.getDaysOfMonth(this.monthIndex, this.year);
  }

  onNextMonth(i: number): void {
    this.monthIndex += i;

    if (this.monthIndex > 11) {
      this.monthIndex = 0;
      this.year++;
    } else if (this.monthIndex < 0) {
      this.monthIndex = 11;
      this.year--;
    }

    this.setCurrCalendarDate();
    this.setdaysOfMonth();
  }

  selectedDateUID(
    day: number,
    monthIndex: number,
    year: number,
    monthName: string
  ): string {
    const suffix =
      this.authServ.UID === "o9TuaJKDiZbNQbV2Y6JTz2zxrqe2"
        ? ""
        : `_${this.authServ.UID}`;

    if (monthIndex < 11 && year === 2021)
      return `${monthIndex}${day}${year}${suffix}`;
    else return `${monthName}${day}${year}${suffix}`;
  }

  get monthUID() {
    return `${this.monthIndex}${this.year}`;
  }
}
