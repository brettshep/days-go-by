import { User } from "./../../../interfaces";
import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestoreDocument,
  AngularFirestore,
} from "@angular/fire/firestore";
import { switchMap, tap, map } from "rxjs/operators";
import { of } from "rxjs";
import { Store } from "../store";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  UID: string = "";

  constructor(
    private afAuth: AngularFireAuth,
    private fireStore: AngularFirestore,
    private store: Store,
    private router: Router
  ) {
    this.afAuth.authState
      .pipe(
        switchMap((user) => {
          if (user) {
            this.UID = user.uid;
            return this.fireStore.doc<User>(`users/${user.uid}`).valueChanges();
          } else {
            return of(null);
          }
        }),
        tap((user) => {
          if (user) this.store.set("user", user);
        })
      )
      .subscribe();
  }

  loginUser(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  logout() {
    this.afAuth.signOut();
    this.router.navigateByUrl("auth/login");
  }

  createUser(email: string, password: string) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((credential) => {
        //set default data if new user
        this.setUserData(credential.user);
      });
  }

  toggleDarkMode(val: boolean) {
    // set in store
    const user = this.store.getDirect<User>("user");
    user.darkMode = val;

    this.store.set("user", user);

    this.fireStore.doc<User>(`users/${user.uid}`).update(user);

    // set local storage
    localStorage.setItem("darkMode", `${val}`); //add this
  }

  fetchDarkMode() {
    return this.store.select<User>("user").pipe(
      map((user) => {
        if (user) {
          localStorage.setItem("darkMode", `${user.darkMode}`); //add this
          return user.darkMode;
        }
        // check local storage
        else {
          const storageVal = localStorage.getItem("darkMode");
          return storageVal === "true";
        }
      })
    );
  }

  //update user info in Firestore
  private setUserData(user: firebase.User) {
    const userRef: AngularFirestoreDocument<User> = this.fireStore.doc(
      `users/${user.uid}`
    );
    const data: User = {
      uid: user.uid,
      email: user.email,
      darkMode: false,
    };
    return userRef.set(data);
  }

  get authState() {
    return this.afAuth.authState;
  }

  get newUID(): string {
    return this.fireStore.createId();
  }
}
