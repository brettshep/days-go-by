import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "./auth.service";
import { map, tap, finalize } from "rxjs/operators";
import { Day } from "../../../interfaces";
import { Store } from "../store";
import { AngularFireStorage } from "@angular/fire/storage";

@Injectable({
  providedIn: "root",
})
export class DayService {
  constructor(
    private fs: AngularFirestore,
    private auth: AuthService,
    private store: Store,
    private storage: AngularFireStorage
  ) {}

  fetchNewDay(dayUID: string) {
    // look in store first
    let storeday = this.store.getDirect<Day>(`${dayUID}_days`);

    // set to null for loading state
    if (!storeday) {
      this.store.set(`${dayUID}_days`, null);
    }

    // get from db
    const fsSub = this.fs
      .collection("users")
      .doc(this.auth.UID)
      .collection("days")
      .doc<Day>(dayUID)
      .valueChanges()
      .pipe(
        tap((day) => {
          if (day) {
            this.store.set(`${dayUID}_days`, day);
          } else {
            this.store.set(`${dayUID}_days`, {});
          }
        })
      )
      .subscribe();

    return this.store.select<Day>(`${dayUID}_days`).pipe(
      finalize(() => {
        fsSub.unsubscribe();
      })
    );
  }

  async updateDay(day: Day, file: File, monthUID: string, dayIndex: number) {
    if (file) {
      const uploadData = await this.uploadImage(file, day.uid);
      const index = day.images.length ? day.images.length - 1 : 0;
      day.images[index].db = uploadData.url;
      day.images[index].uid = uploadData.uid;
      this.setDayData(day, true, monthUID, dayIndex);
    } else {
      this.setDayData(day, true, monthUID, dayIndex);
    }
  }

  async newDay(day: Day, file: File, monthUID: string, dayIndex: number) {
    // check if newday with text or image
    if (file) {
      const uploadData = await this.uploadImage(file, day.uid);
      day.images[0].db = uploadData.url;
      day.images[0].uid = uploadData.uid;
      this.setDayData(day, false, monthUID, dayIndex);
    } else {
      this.setDayData(day, false, monthUID, dayIndex);
    }
  }

  deleteImage(day: Day, index: number, monthUID: string, dayIndex: number) {
    const uidToDelete = day.images[index].uid;

    day.images = day.images.filter((image) => image.uid !== uidToDelete);

    // update firebase and store
    this.setDayData(day, true, monthUID, dayIndex);

    // delete from storage
    const path = `${this.auth.UID}/${day.uid}/${uidToDelete}`;
    this.storage.ref(path).delete();
  }

  setDayData(day: Day, update: boolean, monthUID: string, dayIndex: number) {
    // add to store
    this.store.set(`${day.uid}_days`, day);

    // update firestore
    if (update) {
      this.fs
        .collection("users")
        .doc(this.auth.UID)
        .collection("days")
        .doc<Day>(day.uid)
        .update(day);
    } else {
      this.fs
        .collection("users")
        .doc(this.auth.UID)
        .collection("days")
        .doc<Day>(day.uid)
        .set(day);
    }

    // update month
    const validDay: boolean = !!(day.text || day.images.length);
    let storeMonth = this.store.getDirect<{ [key: string]: boolean }>(
      `${monthUID}_months`
    );
    storeMonth[dayIndex] = validDay;

    this.store.set(`${monthUID}_months`, storeMonth);
    this.fs
      .collection("users")
      .doc(this.auth.UID)
      .collection("months")
      .doc<{ [key: string]: boolean }>(monthUID)
      .update({ [dayIndex]: validDay });
  }

  uploadImage(
    file: File,
    dayid: string
  ): Promise<{ url: string; uid: string }> {
    const uid = this.fs.createId();
    const path = `${this.auth.UID}/${dayid}/${uid}`;
    return new Promise((res, rej) => {
      this.storage
        .upload(path, file, {
          cacheControl: "max-age=31536000",
        })
        .then((storage) => storage.ref.getDownloadURL())
        .catch(rej)
        .then((url) => {
          res({ url, uid });
        });
    });
  }

  fetchDaysOfMonthCompleted(monthUID: string) {
    // look in store first
    let storeMonth = this.store.getDirect<{ [key: string]: boolean }>(
      `${monthUID}_months`
    );

    if (!storeMonth) {
      // set to null for loading state
      this.store.set(`${monthUID}_months`, null);
    }
    // get from db
    const fsSub = this.fs
      .collection("users")
      .doc(this.auth.UID)
      .collection("months")
      .doc<{ [key: string]: boolean }>(monthUID)
      .valueChanges()
      .pipe(
        tap((month) => {
          if (month) {
            this.store.set(`${monthUID}_months`, month);
          } else {
            // set new month in db
            this.fs
              .collection("users")
              .doc(this.auth.UID)
              .collection("months")
              .doc<{ [key: string]: boolean }>(monthUID)
              .set({ created: true });

            this.store.set(`${monthUID}_months`, {});
          }
        })
      )
      .subscribe();

    return this.store
      .select<{ [key: string]: boolean }>(`${monthUID}_months`)
      .pipe(
        finalize(() => {
          fsSub.unsubscribe();
        })
      );
  }
}
