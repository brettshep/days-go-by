import { Injectable } from "@angular/core";
import { CalendarDay } from "../../../interfaces";

@Injectable({
  providedIn: "root",
})
export class CalendarService {
  public monthName: { [key: string]: string } = {
    0: "January",
    1: "February",
    2: "March",
    3: "April",
    4: "May",
    5: "June",
    6: "July",
    7: "August",
    8: "September",
    9: "October",
    10: "November",
    11: "December",
  };

  getDaysOfMonth(_month: number, _year: number) {
    if (_year === undefined || _month === undefined) return;

    const firstday: CalendarDay = this.createDay(1, _month, _year, false);
    const lastday: CalendarDay = this.createDay(0, _month + 1, _year, false);

    let days: CalendarDay[] = [];

    const lastDayOfLastMonth = new Date(_year, _month, 0).getDate();

    //create prior empty days
    for (
      let i = lastDayOfLastMonth - firstday.weekDayIndex + 1;
      i <= lastDayOfLastMonth;
      i++
    ) {
      const month = _month === 0 ? 11 : _month - 1;
      const year = _month === 0 ? _year - 1 : _year;
      const day = this.createDay(i, month, year, true);

      days.push(day);
    }

    // add days of month
    let countDaysInMonth = new Date(_year, _month + 1, 0).getDate();

    for (let i = 1; i < countDaysInMonth + 1; i++) {
      days.push(this.createDay(i, _month, _year, false));
    }

    // add after empty days
    if (days[days.length - 1].weekDayIndex !== 6) {
      let count = 0;
      for (let i = lastday.weekDayIndex; i < 6; i++) {
        count++;
        const month = _month === 11 ? 0 : _month + 1;
        const year = _month === 11 ? _year + 1 : _year;
        const day = this.createDay(count, month, year, true);
        days.push(day);
      }
    }

    return days;
  }

  createDay(
    dayNumber: number,
    monthIndex: number,
    year: number,
    empty: boolean
  ) {
    let uid;
    if (monthIndex < 11 && year === 2021)
      uid = `${monthIndex}${dayNumber}${year}`;
    else uid = `${this.monthName[monthIndex]}${dayNumber}${year}`;

    const day: CalendarDay = {
      number: dayNumber,
      monthIndex,
      year,
      weekDayIndex: new Date(year, monthIndex, dayNumber).getDay(),
      uid,
      empty,
    };

    return day;
  }
}
